import sys
import csv
import json
import urllib

RANGE = range(-90, 0) + range(1, 91);  # 1st and 4th quadrents, and omit 0
URL = "http://euhou.obspm.fr/public/ajax/getFits.php?l=%d&b=0"

for i in RANGE:
  with open("%d_0.csv" % i, "wb") as csvfile:
    print "Downloading lon=%d, lat=0" % i
    raw_data = json.loads(urllib.urlopen(URL % i).read())
    writer = csv.writer(csvfile)
    writer.writerows(raw_data['serie']['data'])


